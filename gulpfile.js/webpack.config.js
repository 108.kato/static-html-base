webpack = require('webpack');
// HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

module.exports = {
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: 'production',
    // mode: 'development',

    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: './_src/js/main.js',
    // ファイルの出力設定
    output: {
        //  出力ファイルのディレクトリ名
        // path: `${__dirname}/dist/assets/js`,
        // path: './dist/assets/js',
        // 出力ファイル名
        filename: './js/app.js'
    },
    module: {
        rules: [{
            // 拡張子 .js の場合
            test: /\.js$/,
            use: [{
                // Babel を利用する
                loader: 'babel-loader',
                // Babel のオプションを指定する
                options: {
                    presets: [
                        // プリセットを指定することで、ES2018 を ES5 に変換
                        '@babel/preset-env',
                    ]
                }
            }]
        }]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        // new HardSourceWebpackPlugin()
    ],
    performance: { hints: false }
};
