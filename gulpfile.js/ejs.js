"use strict";
const gulp = require('gulp');
const ejs = require('gulp-ejs');
const data = require('gulp-data');
const rename = require('gulp-rename');

const path = require('path');
const fs = require('fs');
const pkg = JSON.parse(fs.readFileSync('./package.json'));
// const assetsPath = path.resolve(pkg.path.assetsDir);
const baseDir = path.resolve(pkg.path.baseDir);

// console.log(path);

// const data = {};
const option = {};

const settings = {
    ext: '.html'
};
const jsonData = "_src/ejs/data.json";

function html() {
    console.log("html");
    // var json = JSON.parse(fs.readFileSync(jsonData, 'utf8'));

    return gulp
        .src(['./_src/ejs/**/*.ejs', '!./_src/ejs/**/_*.ejs'])
        .pipe(data(function (file) {
            return { 'filename': file.path }
        }))
        // .pipe(ejs(json, { "ext": ".html" }))
        .pipe(ejs(data, option, settings))
        .pipe(rename({ extname: '.html' }))
        .pipe(gulp.dest(baseDir));
}
exports.html = html;