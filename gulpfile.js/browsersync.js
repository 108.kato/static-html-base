"use strict";
const browserSync = require("browser-sync").create();
const p = require('../package.json');

function browserSyncInit(done) {
    browserSync.init({
        // index: "http://localhost:3000/",
        // startPath: 'index.html',
        port: 3000,
        server: {
            baseDir: p.path.baseDir
        }
    });
    done();
}
function browserSyncReload(done) {
    browserSync.reload();
    done();
}
exports.browserSyncInit = browserSyncInit;
exports.browserSyncReload = browserSyncReload;