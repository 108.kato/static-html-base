"use strict";
const gulp = require('gulp');
const webpackStream = require("webpack-stream");
const webpack = require("webpack");
const webpackConfig = require("./webpack.config");
const p = require('../package.json');

// console.log(p.path);

function webpackScript() {
    console.log("webpackScript");
    return webpackStream(webpackConfig, webpack).on('error', function (e) {
        this.emit('end');
    })
    .pipe(gulp.dest(p.path.assetsDir));
}
exports.webpackScript = webpackScript;